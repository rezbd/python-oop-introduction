from product import Product


# creating a new class that inherit the Product class
class Phone(Product):

    def __init__(self, name: str, price: float, quantity: int = 0, broken_phones=0):
        # call to super function to have access to all attributes / methods
        super().__init__(
            name, price, quantity
        )
        # run validation to the received arguments
        assert broken_phones >= 0, f"Broken Phones {quantity} is not greater or equal to zero!"

        # assign to self object

        self.broken_phones = broken_phones
