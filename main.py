"""Author: A H M Rezwanur Rakib Chy
 Date of Creation: Mar 7, 2022 at 1:14 pm"""

# experimenting on gutter
"""
from product import Product
from phone import Phone


phone1 = Phone("Xiaomi 5 Pro", 17500, 7, 1)

print(Product.all_items)
print(Phone.all_items)
"""

from product import Product

item1 = Product('my item', 750)
item1.name = 'other item'

print(item1.read_only_name)
item1.read_only_name = "BBB"

