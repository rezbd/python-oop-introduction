import csv


class Product:
    payment_rate = 0.80  # payment rate after 20% of discount
    all_items = []

    def __init__(self, name: str, price: float, quantity: int = 0):
        # run validation to the received arguments
        assert price >= 0, f"Price {price} is not greater than or equal to zero!"
        assert quantity >= 0, f"Quantity {quantity} is not greater or equal to zero!"

        # assign to self object
        self.name = name
        self.price = price
        self.quantify = quantity

        # actions to execute
        Product.all_items.append(self)

    def calc_total_price(self):
        return self.price * self.quantify

    def apply_discount(self):
        self.price = self.price * self.payment_rate

    # to read the csv file and instantiate some instances
    @classmethod
    def instantiate_from_csv(cls):
        # 'r' for read only
        with open('items.csv', 'r') as f:
            # to read csv and convert this into a python dictionary
            reader = csv.DictReader(f)
            # to convert the reader into a list
            items = list(reader)

        # to iterate over the items list
        for item in items:
            # to instantiate our instances
            Product(
                name=item.get('name'),
                price=float(item.get('price')),
                quantity=int(item.get('quantity'))
            )

    # static method
    @staticmethod
    def is_integer(num):
        # we will count out the floats that are point zero
        # i.e: 10.0, 5.0
        if isinstance(num, float):
            return num.is_integer()
        # check if it is an integer
        elif isinstance(num, int):
            return True
        else:
            return False

    def __repr__(self):
        return f"{self.__class__.__name__}('{self.name}', {self.price}, {self.quantify})"

    @property
    def read_only_name(self):
        return "AAA"
